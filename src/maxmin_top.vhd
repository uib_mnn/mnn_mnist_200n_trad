
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE work.MyTypes.all;
USE work.maxmin_net_pkg.all;
--============================================================
-- 25 pins in total
-- 
---------------------------------------------------------------
-- Version		Authors				Date				Changes
-- 0.1 			CCFF            
-- 0.2 			CCFF                26/09/2023          Shift Register in the input to minimize
--                                                      pinout
--=============================================================
ENTITY maxmin_top IS	
    PORT
    (
        -- Inputs
        clk     : in std_logic; 
        rst     : in std_logic;
        i_en    : in std_logic;        

        -- X input
        i_x_data        : in std_logic_vector(7 downto 0);
        i_x_wren        : in std_logic;

        --Outputs
        i_y_add     : in  std_logic_vector(3 downto 0);
        o_result    : out std_logic_vector(7 downto 0);
        o_done      : out std_logic
    );
    
END maxmin_top;

ARCHITECTURE arch OF maxmin_top IS     

    signal x_idx : integer range 0 to PKG_MAXMIN_N_INPUTS-1 := 0;
    signal maxmin_i_x : pkg_xi_array_t;
    signal maxmin_i_w : pkg_w_array_t;
    signal lfsr1_seed, lfsr2_seed : STD_LOGIC_VECTOR (PKG_BIT_PRECISSION-1 DOWNTO 0);
    signal maxmin_o_y : pkg_y_slv_array_t;

    signal y_idx : integer range 0 to PKG_MLP_N_OUTPUTS-1;
    signal i_x_wren_tick : std_logic;

    component weights_hardwired IS	
    port
    (
        --Outputs
        weights : out pkg_w_array_t   
    );    
    END component;

    component maxmin_net is
        Port (
            clock, reset, enable: in std_logic;
            lfsr1_seed, lfsr2_seed : in STD_LOGIC_VECTOR (PKG_BIT_PRECISSION-1 DOWNTO 0);
            i_x : in pkg_xi_array_t;
            i_w : in pkg_w_array_t;
            o_y : out pkg_y_slv_array_t;		
            o_done : out std_logic;
            o_next : out std_logic
        );
    end component;

    component edge_detector_up IS     
    port(
        -- inputs
        clk     : in std_logic;
        input   : in std_logic;
        output  : out std_logic
    );
    END component;

BEGIN

    lfsr1_seed <= std_logic_vector(to_unsigned(255, lfsr1_seed'length));
    lfsr2_seed <= std_logic_vector(to_unsigned(53,  lfsr2_seed'length));

    -- Instantation 
    weights_inst: weights_hardwired
    port map(maxmin_i_w);

    maxmin_inst : maxmin_net    
    port map (clk, rst, i_en, 
                lfsr1_seed, 
                lfsr2_seed,
                maxmin_i_x, 
                maxmin_i_w, 
                maxmin_o_y, 
                o_done, 
                open);        

    -- Edge detector instantiation
    edge_detector_inst: edge_detector_up
    port map(clk, i_x_wren, i_x_wren_tick);

    -- X Register Bank
    process(clk)
    BEGIN
        if rising_edge(clk) then     
            if (rst = '1') then
                x_idx <= 0;
            elsif (i_x_wren_tick = '1') then
                if (x_idx < 783) then
                    x_idx <= x_idx + 1;
                end if;
                maxmin_i_x(x_idx) <= signed(i_x_data);
            end if;
        end if;
    end process;

    -- Output, 8 bits MSB
    y_idx <= to_integer(unsigned(i_y_add));
    process(y_idx, maxmin_o_y)
    BEGIN        
        o_result <= std_logic_vector(maxmin_o_y(y_idx)(9 downto 2));                
    end process;

END arch;
