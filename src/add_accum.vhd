-- autogenerate, add_accum v1
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
--use work.MyTypes.all;

ENTITY add_accum IS 
    GENERIC(
      INPUT_WIDTH : INTEGER := 4;
		APC_WIDTH : INTEGER := 4		
    );
    PORT(
        -- inputs
		  clk : in std_logic;
		  reset : in std_logic;
		  
        input 		: in unsigned(INPUT_WIDTH-1 downto 0); 
		  
        -- outputs
        output		: out unsigned(APC_WIDTH-1 downto 0)
   );
END add_accum;

ARCHITECTURE Behavioral OF add_accum IS
    
	signal addnacc_next, addnacc_reg : unsigned (APC_WIDTH -1 downto 0);  
	
BEGIN

   addnacc_next <= addnacc_reg + resize(input, addnacc_next'length);
	
    process (clk)
    begin
        if (rising_edge(clk)) then
            if (reset='1') then
                addnacc_reg <= (others => '0');
            else 
            -- Store accumulation result in a register
                addnacc_reg <= addnacc_next;
            end if;
        end if;
    end process;
	 
	 output <= addnacc_next;
    
END Behavioral;



    
