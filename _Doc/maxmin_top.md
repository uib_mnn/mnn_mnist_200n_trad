
# Entity: maxmin_top 
- **File**: maxmin_top.vhd

## Diagram
![Diagram](maxmin_top.svg "Diagram")
## Ports

| Port name | Direction | Type                         | Description |
| --------- | --------- | ---------------------------- | ----------- |
| clk       | in        | std_logic                    |             |
| rst       | in        | std_logic                    |             |
| i_en      | in        | std_logic                    |             |
| i_x_data  | in        | std_logic_vector(7 downto 0) |             |
| i_x_wren  | in        | std_logic                    |             |
| i_y_add   | in        | std_logic_vector(3 downto 0) |             |
| o_result  | out       | std_logic_vector(7 downto 0) |             |
| o_done    | out       | std_logic                    |             |

## Signals

| Name          | Type                                             | Description |
| ------------- | ------------------------------------------------ | ----------- |
| x_idx         | integer range 0 to PKG_MAXMIN_N_INPUTS-1         |             |
| maxmin_i_x    | pkg_xi_array_t                                   |             |
| maxmin_i_w    | pkg_w_array_t                                    |             |
| lfsr1_seed    | STD_LOGIC_VECTOR (PKG_BIT_PRECISSION-1 DOWNTO 0) |             |
| lfsr2_seed    | STD_LOGIC_VECTOR (PKG_BIT_PRECISSION-1 DOWNTO 0) |             |
| maxmin_o_y    | pkg_y_slv_array_t                                |             |
| y_idx         | integer range 0 to PKG_MLP_N_OUTPUTS-1           |             |
| i_x_wren_tick | std_logic                                        |             |

## Processes
- unnamed: ( clk )
- unnamed: ( y_idx, maxmin_o_y )

## Instantiations

- weights_inst: weights_hardwired
- maxmin_inst: maxmin_net
- edge_detector_inst: edge_detector_up
