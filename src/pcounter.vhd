-- autogenerate, pcounter v1

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
use work.MyTypes.all;

ENTITY pcounter IS 
    GENERIC(
        N_INPUTS : INTEGER := 4        
    );
    PORT(
        -- inputs
        stoch_in: in std_logic_vector(N_INPUTS-1 downto 0); 
        -- outputs
        pc: out unsigned (log2ceil(N_INPUTS+1) -1 DOWNTO 0)
        );
END pcounter;

ARCHITECTURE Behavioral OF pcounter IS
    

BEGIN

    -- Parrallel counter
    -- pc(parallel counter)  = suma de los 1 de 'stoch_in'
    PROCESS (stoch_in)   
        VARIABLE n: INTEGER RANGE 0 TO N_INPUTS;     
    BEGIN
        n:=0;
        FOR i IN stoch_in'RANGE LOOP
            IF stoch_in(i)='1' THEN
                n:=n+1;            
            END IF;
        END LOOP;
        pc<= to_unsigned(n,pc'length);              
    END PROCESS;
  
    
END Behavioral;


