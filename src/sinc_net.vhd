-- autogenerate, sinc_net v1
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
use work.maxmin_net_pkg.all;

entity sinc_net is
	Port (
		clock, reset, enable: IN STD_LOGIC;
		out_bs_tick : out std_logic;
        out_rst_apc_hidden : out std_logic;
        out_rst_apc_lastlayer : out std_logic;
        out_done : out std_logic
	);
end sinc_net;

architecture Behavioral of sinc_net is
	
	-- Senales de sincronismo 
	signal en_buf_out : std_logic;
	-- signal cont_en_buf_out: integer range 0 to PKG_MLP_N_LAYERS-2;
	signal cont_bs : integer range 0 to PKG_TIME_INTEGRATION;
	signal bs_tick : std_logic;
	signal mlp_out_strobe, mlp_out_strobe_next : std_logic;
	signal mlp_done_reg, mlp_next_reg : std_logic := '0';

	
	signal cont_bs_slv : std_logic_vector(PKG_BIT_PRECISSION-1 downto 0);
	signal rst_apc_cont_tick, rst_apc_cont_next_tick : std_logic := '0';
	signal cont_rst_apc_tick : integer range 0 to PCKG_NUM_CHUNKS_APC_EVL_PER_BS-1;
	signal rst_apc_last_layer, rst_apc_last_layer_next : std_logic;
	
begin

	
	--------------------------------------------------------------------------------
	-- Generador de bs_tick
	--------------------------------------------------------------------------------
	-- proceso para saber cuando ha terminado un bitStream
	-- esta se�al nos sirve para leer el siguiente dato x en la entrada
	process(clock)
	BEGIN
		if rising_edge(clock) then
			bs_tick <= '0';
			if (reset='1') or (enable='0') then
				cont_bs <= 0;
			elsif cont_bs = PKG_TIME_INTEGRATION - 2 then
				bs_tick <= '1';
				cont_bs <= cont_bs + 1;
			elsif cont_bs = PKG_TIME_INTEGRATION - 1 then
				cont_bs <= 0;
			else
				cont_bs <= cont_bs + 1;
			end if;
		end if;
	end process;
	out_bs_tick <= bs_tick; -- siguiente senal de entrada
    --------------------------------------------------------------------------------
	-- Generador de rst_apc_hidden
	--------------------------------------------------------------------------------
	-- proceso para contar el periodod en que contaran los apc
	cont_bs_slv <= std_logic_vector(to_unsigned(cont_bs, cont_bs_slv'length));
	rst_apc_cont_next_tick <= '1' when to_integer(unsigned(cont_bs_slv(cont_bs_slv'LEFT - PCKG_LOG_NUM_CHUNKS  downto 0))) = 2**(PKG_BIT_PRECISSION-PCKG_LOG_NUM_CHUNKS)-3 else '0';
	-- se�ales registradas.
	process(clock)
	begin
		if rising_edge(clock) then
			rst_apc_cont_tick <= rst_apc_cont_next_tick;		
		end if;		
	end process;
    out_rst_apc_hidden <= rst_apc_cont_tick;
    


    --------------------------------------------------------------------------------
	-- Generador de rst_apc_lastlayer
	--------------------------------------------------------------------------------
    process(clock)
	BEGIN
		if rising_edge(clock) then
			if (reset='1') or (enable='0') then
				cont_rst_apc_tick <= 0;
			elsif rst_apc_cont_tick='1' then
				if cont_rst_apc_tick = PCKG_NUM_CHUNKS_APC_EVL_PER_BS-1 then   
					cont_rst_apc_tick <= 0;
				else
					cont_rst_apc_tick <= cont_rst_apc_tick + 1;
				end if;
			end if;
		end if;
	end process;
	-- se�al de reset apc para ultimo layer
	rst_apc_last_layer_next <= '1' when (cont_rst_apc_tick = PKG_MLP_N_LAYERS-2)and(rst_apc_cont_tick='1') else '0';
    
    process(clock)
	begin
		if rising_edge(clock) then			
			if PCKG_NUM_CHUNKS_APC_EVL_PER_BS = 1 then
				rst_apc_last_layer <= rst_apc_cont_next_tick;
			else
				rst_apc_last_layer <= rst_apc_last_layer_next;
			end if;			
		end if;		
	end process;

    out_rst_apc_lastlayer <= rst_apc_last_layer;

    --------------------------------------------------------------------------------
	-- Generador de done
	--------------------------------------------------------------------------------
	-- habilitacion de salida de neurona
	-- solo se habilita la salida cuando ya tiene un calculo de la salida
	-- real. la red estocastica tiene un latency de :
	--          Lat_cycles = bitStream * N_Layers    
	-- process(clock)
	-- BEGIN
	-- 	if rising_edge(clock) then
	-- 		if (reset='1') or (enable='0') then
	-- 			en_buf_out <= '0';
	-- 			cont_en_buf_out <= 0;
	-- 		elsif rst_apc_last_layer='1' then				
	-- 			if PCKG_NUM_CHUNKS_APC_EVL_PER_BS = 1 then
	-- 				if cont_en_buf_out = PKG_MLP_N_LAYERS-2 then   
	-- 					en_buf_out <= '1';
	-- 				else
	-- 					en_buf_out <= '0';
	-- 					cont_en_buf_out <= cont_en_buf_out + 1;
	-- 				end if;					
	-- 			else
	-- 				if en_buf_out = '0' then   
	-- 					en_buf_out <= '1';	
	-- 				end if;
	-- 			end if;
				
	-- 		end if;
	-- 	end if;
	-- end process;
	en_buf_out <= '1';
	mlp_out_strobe_next <= en_buf_out and rst_apc_last_layer;

	-- strobe done
	process(clock)
	BEGIN
		if rising_edge(clock) then
			mlp_out_strobe <= mlp_out_strobe_next;
			mlp_done_reg <= mlp_out_strobe;
		end if;
	end process;
	out_done <=  mlp_done_reg;
	
	

END Behavioral;






