LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE work.maxmin_net_pkg.all;
USE work.MyTypes.all;

---------------------------------------------------------------
-- Version		Authors				Date				Changes
---------------------------------------------------------------
-- 0.1 			CCFF             27/10/2021          	Initial

--  constant MAXMIN_NEURON_TYPE_MAX : integer := 0;
-- 	constant MAXMIN_NEURON_TYPE_MIN : integer := 1;

--=============================================================

ENTITY neurona_maxmin IS
	generic (				
		GNRC_N_INPUTS                   : integer   :=4; 
		GNRC_MAXMIN_TYPE                : INTEGER   := 0;        
        GNRC_ADDITION_SCALE_DOWN        : INTEGER   := 2
	);
	PORT
	(
		-- Inputs
		clock : IN STD_LOGIC; 
		reset : IN STD_LOGIC;		
		
		i_lfsr : 		IN signed (PKG_BIT_PRECISSION-1 DOWNTO 0);
		i_x : 			IN pkg_xi_neuron_maxmin_array_t(0 to GNRC_N_INPUTS-1);
		i_w : 			IN pkg_xi_neuron_maxmin_array_t(0 to GNRC_N_INPUTS-1);
	
		--Outputs
		o_y_sc : 		OUT STD_LOGIC		
	);
	
END neurona_maxmin;

ARCHITECTURE arch OF neurona_maxmin IS
	constant MAXMIN_NEURON_TYPE_MAX : integer := 0;
	constant MAXMIN_NEURON_TYPE_MIN : integer := 1;

	--signal sum_array            : pkg_sum_array_t           (GNRC_N_INPUTS - 1 downto 0);
	signal sum_array_scaled     : pkg_sum_scaled_array_t    (GNRC_N_INPUTS - 1 downto 0);
	signal sum_sc               : std_logic_vector          (GNRC_N_INPUTS - 1 downto 0);

BEGIN

	------------------------------------------------
	-- Addition Layer
	------------------------------------------------
	process (i_x, i_w)
		variable TMP_SUM : signed (PKG_INPUT_WIDTH_BITS downto 0):=(others=>'0');
	begin		
		for i in 0 to GNRC_N_INPUTS-1 loop
            
            TMP_SUM := resize(i_x(i), TMP_SUM'length) + resize(i_w(i), TMP_SUM'length) ;
            
            -- CLAMP
            -- if TMP_SUM > (2**(PKG_BIT_PRECISSION-1)-1) then
            -- 	TMP_SUM := to_signed(2**(PKG_BIT_PRECISSION-1)-1 ,TMP_SUM'length);
            -- end if;
            -- if TMP_SUM < -(2**(PKG_BIT_PRECISSION-1)-1) then
            -- 	TMP_SUM := to_signed(-1*(2**(PKG_BIT_PRECISSION-1)-1) ,TMP_SUM'length);
            -- end if;

            --sum_array(i)          <= TMP_SUM;
            sum_array_scaled(i)   <= resize(TMP_SUM/GNRC_ADDITION_SCALE_DOWN, i_lfsr'length);                        
		end loop;		
	end process;

	------------------------------------------------
	-- SUM conversion to SC
	------------------------------------------------
	b2p_sum_inst: FOR index IN sum_sc'RANGE GENERATE 
		sum_sc(index) <= '1' when sum_array_scaled(index) >= i_lfsr else '0';
	END GENERATE;

	------------------------------------------------
	-- MAX or MIN Output SC 
	------------------------------------------------
	gen_max_output: if GNRC_MAXMIN_TYPE = MAXMIN_NEURON_TYPE_MAX generate
		process (sum_sc)
			variable TMP : std_logic;
		begin
			TMP := '0';
			for I in sum_sc'RANGE loop
				TMP := TMP or sum_sc(I);
			end loop;
			o_y_sc <= TMP;
		end process;		
	end generate gen_max_output;

	gen_min_output: if GNRC_MAXMIN_TYPE = MAXMIN_NEURON_TYPE_MIN generate
		process (sum_sc)
			variable TMP : std_logic;
		begin
			TMP := '1';
			for I in sum_sc'RANGE loop
				TMP := TMP and sum_sc(I);
			end loop;
			o_y_sc <= TMP;
		end process;		
	end generate gen_min_output;
	

END arch;
