-- autogenerate, MyTypes v1
--============================================================
-- Design Unit: 
-- Purpose: 
-- 
-- Authors:  CCFF
-- WEB info: 
---------------------------------------------------------------
---------------------------------------------------------------
-- Version		Authors				Date				Changes
-- 0.1 			CCFF            11/12/2018              Initial
--=============================================================

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE IEEE.Numeric_Std.ALL;


package mytypes is


    --Matriz de 1 dimensiones de 8 bits
    type uint8_t is array(natural range <>) of std_logic_vector(7 downto 0);
    --Matriz de 1 dimensiones de 1 bits
    type uint1_t is array(natural range <>) of std_logic;
    --Matriz de 2 dimensiones de 8 bits
    type matrix2D_8_t is array(natural range <>, natural range <>) of std_logic_vector(7 downto 0);
    --Matriz de 3 dimensiones de 8 bit
    type matrix3D_8_t is array(natural range <>, natural range <>, natural range <>) of std_logic_vector(7 downto 0);
    --matriz de 4 dimensionestype matrix4D_8_t is array(natural range <>, natural range <>, natural range <>, natural range <>) of std_logic_vector(7 downto 0);
    --Matriz de 3 dimensiones de 1 bit
    type matrix3D_1_t is array(natural range <>, natural range <>, natural range <>) of std_logic;
	
	--declaracion de funciones
	function srlInteger(arg: integer; s:natural) return integer;
	function log2ceil (L: POSITIVE) return NATURAL;
end mytypes;

-- Package Body Section
package body mytypes is
    function srlInteger(arg: integer; s:natural) return integer is
    begin
        return to_integer(SHIFT_RIGHT(to_UNSIGNED(ARG,32), s));
    end srlInteger;
	 
    -- For log2 aproximation in signals.
    -- Example use:
    --      bin_out: out std_logic_vector (log2ceil(N)-1 downto 0)
    function log2ceil (L: POSITIVE) return NATURAL is
        variable i, bitCount : natural;
    begin
        i := L-1;
        bitCount:=0;
        while (i > 0) loop
            bitCount := bitCount + 1;
            i:=srlInteger(i,1);
        end loop;
        return bitCount;
    end log2ceil;
    
end package body mytypes;

