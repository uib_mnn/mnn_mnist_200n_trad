
`timescale 1 ns/10 ps  // time-unit = 1 ns, precision = 10 ps

module tb_maxmin_top_verilog();
    
    localparam T = 20; 
    reg clk;
    reg rst;
    reg i_en;
    reg [7:0] i_x_data;
    reg i_x_wren;
    reg [3:0] i_y_add;
    wire [7:0] o_result;
    wire o_done;

    integer i;
    integer file1;

    maxmin_top dut (
     .clk (clk),
     .rst (rst),
     .i_en (i_en),
     .i_x_data (i_x_data),
     .i_x_wren (i_x_wren),
     .i_y_add(i_y_add),
     .o_result (o_result),
     .o_done (o_done)
    );

    
    // clock generation
    initial begin
        clk = 1'b0;
        forever #(T/2) clk = ~clk;
    end

    // signal stimulus
    initial begin
        i_en = 1'b0;
        rst = 1'b1;
        i_x_data = 8'h00;
        i_x_wren = 1'b0;
        i_y_add  = 4'b0;
        file1=$fopen("x_test_img0.csv","r");

        #(1*T);
        rst = 1'b0;          

        for (i=0; i<784; i=i+1) begin
            $fscanf(file1,"%d", i_x_data);
            #(1*T);
            i_x_wren = 1'b1;
            #(4*T);
            i_x_wren = 1'b0;
        end
        $fclose(file1);   

        // do a reset for the device before starting to operate
        rst = 1'b1;
        #(2*T);
        rst = 1'b0;

        i_en = 1'b1;
        wait (o_done == 1);
        #(3*T);

        for (i=0; i<9; i=i+1) begin
            i_y_add <= i_y_add + 1;
            #(2*T);
        end

    end

endmodule
